package com.little.test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class LinedHashMapTest {
	
	public static void main(String[]args){
		
		HashMap<String, String> hashMap = new HashMap<String, String>(); 
		LinkedHashMap<String, String> linkedMap = new LinkedHashMap<String, String>();
		
		//---输入数据
		for(int i = 0; i < 10; i++){
			
			hashMap.put("" + i, "" + i);
			linkedMap.put("" + i, "" + i);
			
		}
		
		//---输出HashMap中的数据
		System.out.println("HashMap:");
		for(Entry<String, String> entry : hashMap.entrySet()){
			
			System.out.print(entry.getKey() + " ");
			
		}
		
		System.out.println();
		
		//---输出LinkedMap中的数据
		System.out.println("LinkedHashMap:");
		for(Entry<String, String> entry : linkedMap.entrySet()){
			
			System.out.print(entry.getKey() + " ");
			
		}
		
	}

}

//控制台输出结果：

HashMap:
3 2 1 0 7 6 5 4 9 8 
LinkedHashMap:
0 1 2 3 4 5 6 7 8 9 

/*
总结：
         根据结果咱就知道了：原来LinkedHashMap就是一个有顺序的HashMap嘛！一般用的最多就是   HashMap了，但是如果遇到有顺序要求的情况，就可以使用LinkedHashMap！
*/
  
/*
   问题：
           为啥HashMap不管输出多少次，结果都是“3 2 1 0 7 6 5 4 9 8 ”？
           难道是Hash算法！
*/